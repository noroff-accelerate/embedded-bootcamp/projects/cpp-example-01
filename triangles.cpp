// Author: Craig Marais
// Craig's Triangle Printer v2.1

// Header file for io (input/output) functions
#include <iostream>

// The main function - entry point
int main()
{
    int size = 9;
    std::cout<<"Craigs Triangle Printer"<<std::endl;

    for (int height = 0; height < size; height++)
    {
        for (int width = 0; width < height; width++)
        {
        if((height + width) == size)
        {
            std::cout<<'#';
        }
        else
        {
            std::cout<<'*';
        }
        }
        std::cout<<std::endl;
    }

    return 0;
}